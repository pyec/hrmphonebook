﻿using HRMPhoneBook.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace HRMPhoneBook.Cache
{
    [System.ComponentModel.DataObject]
    public static class StaticCache
    {
        public static void LoadStaticStreetCache()
        {
            var streets = GetStreetsFromOpenData();

            HttpRuntime.Cache.Insert(
          /* key */                "streetsSelectList",
          /* value */              streets,
          /* dependencies */       null,
          /* absoluteExpiration */ DateTime.Now.AddDays(1),
          /* slidingExpiration */  System.Web.Caching.Cache.NoSlidingExpiration,
          /* priority */           CacheItemPriority.NotRemovable,
          /* onRemoveCallback */   new CacheItemRemovedCallback (RefreshStreetData));
        }

        public static List<SelectListItem> GetStreetsFromOpenData()
        {
            var remoteUri = ConfigurationManager.AppSettings["GISDataURL"];
            var streets = new List<SelectListItem>();
            var streetVMs = new List<StreetVM>();

            // Create a new WebClient instance.
            using (var webClient = new WebClient())
            {
                var proxyURL = ConfigurationManager.AppSettings["proxyURL"];
                //var wp = new WebProxy(proxyURL);
                //webClient.Proxy = wp;
                // Download the Web resource and save it into a data buffer.
                var myDataBuffer = webClient.DownloadData(remoteUri);

                // Display the downloaded data.
                var downloadedData = Encoding.ASCII.GetString(myDataBuffer);

                var jsonData = JObject.Parse(downloadedData);
                var streetJsonData = jsonData["features"];

                foreach (var item in streetJsonData)
                {
                    var streetId = item["properties"]["STR_CODE_L"].ToString();
                    var streetName = item["properties"]["STR_NAME"].ToString();
                    var streetType = item["properties"]["STR_TYPE"].ToString();
                    var city = item["properties"]["GSA_LEFT"].ToString();

                    if (streetName.Length > 0)
                    {
                        var streetVM = new StreetVM
                        {
                            StreetId = streetId,
                            StreetName = streetName + " " + streetType + ", " + city
                        };

                        streetVMs.Add(streetVM);
                    }
                }
            }

            var sortedStreets = streetVMs.OrderBy(x => x.StreetName).ToList();

            foreach (var street in sortedStreets)
            {
                var streetItem = new SelectListItem
                {
                    Value = street.StreetId,
                    Text = street.StreetName
                };

                streets.Add(streetItem);
            }

            var streetTip = new SelectListItem
            {
                Value = null,
                Text = "--- Select Street ---"
            };

            streets.Insert(0, streetTip);

            return streets;
        }

        static void RefreshStreetData(String key, Object item,
              CacheItemRemovedReason reason)
        {
            var streets = GetStreetsFromOpenData();

            HttpRuntime.Cache.Insert(
          /* key */                "streetsSelectList",
          /* value */              streets,
          /* dependencies */       null,
          /* absoluteExpiration */ DateTime.Now.AddDays(1),
          /* slidingExpiration */  System.Web.Caching.Cache.NoSlidingExpiration,
          /* priority */           CacheItemPriority.NotRemovable,
          /* onRemoveCallback */   new CacheItemRemovedCallback(RefreshStreetData));
        }


        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static List<SelectListItem> GetOpenDataStreets()
        {
            return (List <SelectListItem>)HttpRuntime.Cache["streetsSelectList"];
        }
    }
}