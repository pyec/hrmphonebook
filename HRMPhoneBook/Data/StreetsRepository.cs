﻿using HRMPhoneBook.Cache;
using HRMPhoneBook.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HRMPhoneBook.Data
{
    public static class StreetsRepository
    {
        public static IEnumerable<SelectListItem> GetStreets()
        {
            //var remoteUri = "https://opendata.arcgis.com/datasets/6d93ecf9e2a348b891e1d5f21c436fe1_0.geojson";
            //var streets = new List<SelectListItem>();
            //var streetVMs = new List<StreetVM>();

            //// Create a new WebClient instance.
            //using (var webClient = new WebClient())
            //{
            //    var wp = new WebProxy("http://proxy.halifax.ca:8002");
            //    webClient.Proxy = wp;
            //    // Download the Web resource and save it into a data buffer.
            //    var myDataBuffer = webClient.DownloadData(remoteUri);

            //    // Display the downloaded data.
            //    var downloadedData = Encoding.ASCII.GetString(myDataBuffer);

            //    var jsonData = JObject.Parse(downloadedData);
            //    var streetJsonData = jsonData["features"];

            //    foreach (var item in streetJsonData)
            //    {
            //        var streetId = item["properties"]["STR_CODE_L"].ToString();
            //        var streetName = item["properties"]["STR_NAME"].ToString();
            //        var streetType = item["properties"]["STR_TYPE"].ToString();
            //        var city = item["properties"]["GSA_LEFT"].ToString();

            //        if (streetName.Length > 0)
            //        {
            //            var streetVM = new StreetVM
            //            {
            //                StreetId = streetId,
            //                StreetName = streetName + " "  + streetType + ", " + city
            //            };

            //            streetVMs.Add(streetVM);
            //        }
            //    }
            //}

            //var sortedStreets = streetVMs.OrderBy(x => x.StreetName).ToList();

            //foreach (var street in sortedStreets)
            //{
            //    var streetItem = new SelectListItem
            //    {
            //        Value = street.StreetId,
            //        Text = street.StreetName
            //    };

            //    streets.Add(streetItem);
            //}

            var streets = StaticCache.GetOpenDataStreets();

            return new SelectList(streets, "Value", "Text");
        }

        public static IEnumerable<SelectListItem> GetStreetsFromGIS()
        {

            var remoteUri = "https://opendata.arcgis.com/datasets/6d93ecf9e2a348b891e1d5f21c436fe1_0.geojson";

            //var streets = new List<StreetVM>();

            var streets = new List<SelectListItem>();

            // Create a new WebClient instance.
            using (var webClient = new WebClient())
            {
                var wp = new WebProxy("http://proxy.halifax.ca:8002");
                webClient.Proxy = wp;
                // Download the Web resource and save it into a data buffer.
                var myDataBuffer = webClient.DownloadData(remoteUri);

                // Display the downloaded data.
                var downloadedData = Encoding.ASCII.GetString(myDataBuffer);

                var jsonData = JObject.Parse(downloadedData);
                var streetJsonData = jsonData["features"];

                foreach (var item in streetJsonData)
                {
                    var streetId = item["properties"]["STR_CODE_L"];
                    var streetName = item["properties"]["MIX_FULL"];

                    //var street = new StreetVM
                    //{
                    //    StreetId = Convert.ToInt32(streetId),
                    //    StreetName = streetName.ToString()
                    //};

                    //streets.Add(street);

                    var street = new SelectListItem
                    {
                        Value = streetId.ToString(),
                        Text = streetName.ToString()
                    };

                    streets.Add(street);
                }
            }

            return new SelectList(streets, "Value", "Text").OrderBy(x => x.Text);

        }
    }
}