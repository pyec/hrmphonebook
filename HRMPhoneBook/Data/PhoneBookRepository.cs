﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HRMPhoneBook.ViewModels;

namespace HRMPhoneBook.Data
{
    public class PhoneBookRepository
    {
        public static PhoneIndexVM Index()
        {
            var phoneBookSearch = new PhoneIndexVM
            {
                BusinessUnits = BusinessUnitsRepository.GetBusinessUnits(),
                Streets = StreetsRepository.GetStreets(),
                SearchResults = new List<HRMPhoneVM>()
            };

            return phoneBookSearch;
        }

        public static List<HRMPhoneVM> Search(PhoneIndexVM viewModel)
        {
            using (var context = new PhoneBookEntities())
            {
                var search = from p in context.HRM_PHONE select p;

                if (viewModel.LastName != null)
                {
                    search = search.Where(p => p.LAST_NME.StartsWith(viewModel.LastName));
                }

                if (viewModel.FirstName != null)
                {
                    search = search.Where(p => p.FIRST_NME.StartsWith(viewModel.FirstName));
                }

                if (viewModel.Title != null)
                {
                    search = search.Where(p => p.TITLE_DSC.StartsWith(viewModel.Title));
                }

                if (viewModel.Email != null)
                {
                    search = search.Where(p => p.EMAIL_ID.StartsWith(viewModel.Email));
                }

                if (viewModel.Section != null)
                {
                    search = search.Where(p => p.SECTION_DSC.StartsWith(viewModel.Section));
                }

                if (viewModel.WorkLocation != null)
                {
                    search = search.Where(p => p.LOCATION_DSC.StartsWith(viewModel.WorkLocation));
                }

                if (viewModel.SelectedBusinessUnit != null)
                {
                    var selectedBusinessUnit = Convert.ToInt32(viewModel.SelectedBusinessUnit);
                    search = search.Where(p => p.BUSUNIT_CDE == selectedBusinessUnit);
                }

                if (viewModel.CivicNumber != null)
                {
                    search = search.Where(p => p.CIVIC_NBR.StartsWith(viewModel.CivicNumber));
                }

                if (viewModel.SelectedStreet != null)
                {
                    var selectedStreet = Convert.ToInt32(viewModel.SelectedStreet);
                    search = search.Where(p => p.STR_CODE == selectedStreet);
                }

                if (viewModel.Floor != null)
                {
                    search = search.Where(p => p.FLOOR.StartsWith(viewModel.Floor));
                }

                if (viewModel.WorkPhone != null)
                {
                    search = search.Where(p => p.WORK_PHN.StartsWith(viewModel.WorkPhone));
                }

                if (viewModel.Cell != null)
                {
                    search = search.Where(p => p.CELL_PHN.StartsWith(viewModel.Cell));
                }

                if (viewModel.Pager != null)
                {
                    search = search.Where(p => p.PAGER_PHN.StartsWith(viewModel.Pager));
                }

                if (viewModel.Fax != null)
                {
                    search = search.Where(p => p.FAX_PHN.StartsWith(viewModel.Fax));
                }

                var searchResults = search.OrderBy(p => p.LAST_NME).OrderBy(p => p.FIRST_NME).ToList();

                var phoneRecords = new List<HRMPhoneVM>();
                    
                foreach (var record in searchResults)
                {
                    var phoneRecord = new HRMPhoneVM
                    {
                        SEQ_ID = record.SEQ_ID,
                        LAST_NME = record.LAST_NME,
                        FIRST_NME = record.FIRST_NME,
                        BUSUNIT_CDE = record.BUSUNIT_CDE,
                        WORK_PHN = record.WORK_PHN,
                        EMAIL_ID = record.EMAIL_ID
                    };

                    var businessUnit = BusinessUnitsRepository.Find(record.BUSUNIT_CDE);

                    if (businessUnit != null)
                    {
                        phoneRecord.BusinessUnitDescription = businessUnit.BUSUNIT_DSC;
                    }

                    phoneRecords.Add(phoneRecord);
                }

                return phoneRecords;
            }
        }

        public static HRMPhoneVM Find(int id)
        {
            using (var context = new PhoneBookEntities())
            {
                var searchResults = context.HRM_PHONE
                    .Where(p => p.SEQ_ID == id)
                    .Select(p =>
                   new HRMPhoneVM
                   {
                       SEQ_ID = p.SEQ_ID,
                       EMPL_ID = p.EMPL_ID,
                       LAST_NME = p.LAST_NME,
                       FIRST_NME = p.FIRST_NME,
                       TITLE_DSC = p.TITLE_DSC,
                       BUNIT_DSC = p.BUNIT_DSC,
                       DIVISION_DSC = p.DIVISION_DSC,
                       SECTION_DSC = p.SECTION_DSC,
                       LOCATION_DSC = p.LOCATION_DSC,
                       WORK_PHN = p.WORK_PHN,
                       FAX_PHN = p.FAX_PHN,
                       CELL_PHN = p.CELL_PHN,
                       PAGER_PHN = p.PAGER_PHN,
                       UPDATE_DTE = p.UPDATE_DTE,
                       EMAIL_ID = p.EMAIL_ID,
                       BUSUNIT_CDE = p.BUSUNIT_CDE,
                       STR_CODE = p.STR_CODE,
                       CIVIC_NBR = p.CIVIC_NBR,
                       FLOOR = p.FLOOR
                   }).FirstOrDefault();

                return searchResults;
            }
        }
    }
}