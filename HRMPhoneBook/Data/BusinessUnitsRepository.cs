﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace HRMPhoneBook.Data
{
    public class BusinessUnitsRepository
    {
        public static IEnumerable<SelectListItem> GetBusinessUnits()
        {
            using (var context = new PhoneBookEntities())
            {
                var businessUnits = context.HRM_BUSUNIT.AsNoTracking()
                    .Where(b => b.CURRENT_FLG == "Y")
                    .OrderBy(b => b.BUSUNIT_DSC)
                    .Select(b =>
                   new SelectListItem
                   {
                       Value = b.BUSUNIT_CDE.ToString(),
                       Text = b.BUSUNIT_DSC
                   }).ToList();

                var businessUnitTip = new SelectListItem
                {
                    Value = null,
                    Text = "--- Select Business Unit ---"
                };

                businessUnits.Insert(0, businessUnitTip);

                return new SelectList(businessUnits, "Value", "Text");
            }
        }

        public static HRM_BUSUNIT Find(int? id)
        {
            using (var context = new PhoneBookEntities())
            {
                var businessUnit = context.HRM_BUSUNIT
                    .FirstOrDefault(b => b.BUSUNIT_CDE == id);

                return businessUnit;
            }
        }
    }
}