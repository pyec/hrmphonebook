﻿using System;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using HRMPhoneBook.Data;
using HRMPhoneBook.ViewModels;
using System.DirectoryServices.AccountManagement;
using System.Configuration;
using System.Net.Mail;
using HRMPhoneBook.Cache;
using System.Security.Principal;

namespace HRMPhoneBook.Controllers
{
    public class HRM_PHONEController : Controller
    {
        private PhoneBookEntities db = new PhoneBookEntities();

        // GET: HRM_PHONE
        public ActionResult Index()
        {
            try
            {
                var repo = new PhoneBookRepository();
                var indexView = PhoneBookRepository.Index();

                ViewBag.SearchClicked = false;

                return View(indexView);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Search(PhoneIndexVM viewModel)
        {
            try { 
                if (ModelState.IsValid)
                {
                    var repo = new PhoneBookRepository();

                    var searcResults = PhoneBookRepository.Search(viewModel);

                    viewModel.SearchResults = searcResults;

                    viewModel.BusinessUnits = BusinessUnitsRepository.GetBusinessUnits();
                    viewModel.Streets = StreetsRepository.GetStreets();

                    ViewBag.SearchClicked = true;

                    return View(nameof(Index), viewModel);
                }

                viewModel.BusinessUnits = BusinessUnitsRepository.GetBusinessUnits();
                viewModel.Streets = StreetsRepository.GetStreets();

                ViewBag.SearchClicked = true;

                return View(nameof(Index), viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: HRM_PHONE/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                if (id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var repo = new PhoneBookRepository();
                var viewModel = PhoneBookRepository.Find(id);

                if (viewModel.SEQ_ID == 0)
                {
                    return HttpNotFound();
                }

                viewModel.BusinessUnits = BusinessUnitsRepository.GetBusinessUnits();
                viewModel.Streets = StreetsRepository.GetStreets();

                viewModel.PageMode = nameof(Edit);
                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
            }

        // GET: HRM_PHONE/Create
        public ActionResult Create()
        {
            try { 
                var viewModel = new HRMPhoneVM
                {
                    BusinessUnits = BusinessUnitsRepository.GetBusinessUnits(),
                    Streets = StreetsRepository.GetStreets(),

                    PageMode = nameof(Create)
                };
                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: HRM_PHONE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EMPL_ID,SEQ_ID,LAST_NME,FIRST_NME,TITLE_DSC,BUNIT_DSC,DIVISION_DSC,SECTION_DSC,LOCATION_DSC,WORK_PHN,FAX_PHN,CELL_PHN,PAGER_PHN,UPDATE_DTE,EMAIL_ID,BUSUNIT_CDE,STR_CODE,CIVIC_NBR,FLOOR")] HRM_PHONE hRM_PHONE)
        {
            try
            {
                var viewModel = new HRMPhoneVM
                {
                    EMPL_ID = hRM_PHONE.EMPL_ID,
                    SEQ_ID = hRM_PHONE.SEQ_ID,
                    LAST_NME = hRM_PHONE.LAST_NME,
                    FIRST_NME = hRM_PHONE.FIRST_NME,
                    TITLE_DSC = hRM_PHONE.TITLE_DSC,
                    BUNIT_DSC = hRM_PHONE.BUNIT_DSC,
                    DIVISION_DSC = hRM_PHONE.DIVISION_DSC,
                    SECTION_DSC = hRM_PHONE.SECTION_DSC,
                    LOCATION_DSC = hRM_PHONE.LOCATION_DSC,
                    WORK_PHN = hRM_PHONE.WORK_PHN,
                    FAX_PHN = hRM_PHONE.FAX_PHN,
                    CELL_PHN = hRM_PHONE.CELL_PHN,
                    PAGER_PHN = hRM_PHONE.PAGER_PHN,
                    UPDATE_DTE = hRM_PHONE.UPDATE_DTE,
                    EMAIL_ID = hRM_PHONE.EMAIL_ID,
                    BUSUNIT_CDE = hRM_PHONE.BUSUNIT_CDE,
                    STR_CODE = hRM_PHONE.STR_CODE,
                    CIVIC_NBR = hRM_PHONE.CIVIC_NBR,
                    FLOOR = hRM_PHONE.FLOOR,
                    BusinessUnits = BusinessUnitsRepository.GetBusinessUnits(),
                    Streets = StreetsRepository.GetStreets(),
                    PageMode = nameof(Create)
                };

                if (ModelState.IsValid)
                {
                    // if user is an phone book admin do the same 
                    // otherwise send them to a landing page
                    // to verify the create
                    if (IsPhoneBookAdmin())
                    {
                        hRM_PHONE.UPDATE_DTE = DateTime.Now;  //update the date 

                        db.HRM_PHONE.Add(hRM_PHONE);
                        db.SaveChanges();
                        //return RedirectToAction(nameof(Index));

                        return RedirectToAction(nameof(Edit), new { id = hRM_PHONE.SEQ_ID.ToString(), message = "Phone Record Created" });
                    }
                    else
                    {
                        viewModel.ActionMode = "CREATE";
                        return RedirectToAction(nameof(Review), viewModel);
                    }

                }

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
}

        private bool IsPhoneBookAdmin()
        {
            try
            {
                using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain))
                {
                    var userName = User.Identity.Name;
                    var isAdmin = false;

                    // find a user
                    var user = UserPrincipal.FindByIdentity(ctx, userName);

                    // find the group in question
                    var adminADGroup = ConfigurationManager.AppSettings["adminADGroup"];
                    var group = GroupPrincipal.FindByIdentity(ctx, adminADGroup);

                    if (user != null && group != null)
                    {
                        // get the authorization groups - those are the "roles" 
                        var groups = user.GetAuthorizationGroups();

                        //foreach (Principal principal in groups)
                        //{
                        //    // do something with the group (or role) in question
                        //}

                        // check if user is member of the Global AD group
                        if (user.IsMemberOf(group))
                        {
                            isAdmin = true;
                        }
                    }

                    if (!isAdmin)
                    {
                        //check if in local AD group
                        isAdmin = IsInGroup(userName, adminADGroup);
                    }


                    return isAdmin;
                }
            }
            catch (Exception)
            {
                throw;
            }
}

        static bool IsInGroup(string user, string group)
        {
            if (user.Contains("\\"))
            {
                user = user.Substring(user.IndexOf("\\") + 1);
            }

            using (WindowsIdentity identity = new WindowsIdentity(user))
            {
                var principal = new WindowsPrincipal(identity);
                return principal.IsInRole(group);
            }
        }

        // GET: HRM_PHONE/Edit/5
        public ActionResult Edit(int id, string message = null)
        {
            try
            {
                var repo = new PhoneBookRepository();
                var viewModel = PhoneBookRepository.Find(id);

                if (viewModel.SEQ_ID == 0)
                {
                    return HttpNotFound();
                }

                viewModel.BusinessUnits = BusinessUnitsRepository.GetBusinessUnits();
                viewModel.Streets = StreetsRepository.GetStreets();

                viewModel.PageMode = nameof(Edit);
                viewModel.PageMessage = message;
                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: HRM_PHONE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EMPL_ID,SEQ_ID,LAST_NME,FIRST_NME,TITLE_DSC,BUNIT_DSC,DIVISION_DSC,SECTION_DSC,LOCATION_DSC,WORK_PHN,FAX_PHN,CELL_PHN,PAGER_PHN,UPDATE_DTE,EMAIL_ID,BUSUNIT_CDE,STR_CODE,CIVIC_NBR,FLOOR")] HRM_PHONE hRM_PHONE)
        {
            try
            {
                var viewModel = new HRMPhoneVM
                {
                    EMPL_ID = hRM_PHONE.EMPL_ID,
                    SEQ_ID = hRM_PHONE.SEQ_ID,
                    LAST_NME = hRM_PHONE.LAST_NME,
                    FIRST_NME = hRM_PHONE.FIRST_NME,
                    TITLE_DSC = hRM_PHONE.TITLE_DSC,
                    BUNIT_DSC = hRM_PHONE.BUNIT_DSC,
                    DIVISION_DSC = hRM_PHONE.DIVISION_DSC,
                    SECTION_DSC = hRM_PHONE.SECTION_DSC,
                    LOCATION_DSC = hRM_PHONE.LOCATION_DSC,
                    WORK_PHN = hRM_PHONE.WORK_PHN,
                    FAX_PHN = hRM_PHONE.FAX_PHN,
                    CELL_PHN = hRM_PHONE.CELL_PHN,
                    PAGER_PHN = hRM_PHONE.PAGER_PHN,
                    UPDATE_DTE = hRM_PHONE.UPDATE_DTE,
                    EMAIL_ID = hRM_PHONE.EMAIL_ID,
                    BUSUNIT_CDE = hRM_PHONE.BUSUNIT_CDE,
                    STR_CODE = hRM_PHONE.STR_CODE,
                    CIVIC_NBR = hRM_PHONE.CIVIC_NBR,
                    FLOOR = hRM_PHONE.FLOOR,
                    BusinessUnits = BusinessUnitsRepository.GetBusinessUnits(),
                    Streets = StreetsRepository.GetStreets(),
                    PageMode = nameof(Edit)
                };

                if (ModelState.IsValid)
                {
                    // if user is an phone book admin do the same 
                    // otherwise send them to a landing page
                    // to verify the edit
                    if (IsPhoneBookAdmin())
                    {
                        hRM_PHONE.UPDATE_DTE = DateTime.Now;  //update the date 

                        db.Entry(hRM_PHONE).State = EntityState.Modified;
                        db.SaveChanges();

                        viewModel.PageMessage = "Phone Record Updated";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        viewModel.ActionMode = "EDIT";
                        return RedirectToAction(nameof(Review), viewModel);
                    }
                }

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: HRM_PHONE/Review
        public ActionResult Review(HRMPhoneVM hRM_PHONE)
        {
            try
            {
                return View(hRM_PHONE);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: HRM_PHONE/SendEmail
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendEmail(HRMPhoneVM hRM_PHONE)
        {
            try
            {
                var action = hRM_PHONE.ActionMode;
                var subject = "";
                var businessUnitRespository = new BusinessUnitsRepository();

                if (action == "DELETE")
                {
                    subject = ConfigurationManager.AppSettings["emailDeleteSubject"];
                }
                else
                {
                    subject = ConfigurationManager.AppSettings["emailSubject"];
                }

                var businessUnit = new HRM_BUSUNIT();

                if (hRM_PHONE.BUSUNIT_CDE != default(int))
                {
                    businessUnit = BusinessUnitsRepository.Find(hRM_PHONE.BUSUNIT_CDE);
                }

                var smtpMailer = ConfigurationManager.AppSettings["smtpMailer"];
                var smtpPort = ConfigurationManager.AppSettings["smtpPort"];

                var businessUnitDescription = "";
                var street = hRM_PHONE.STR_CODE;       
                var lastName = hRM_PHONE.LAST_NME;
                var firstName = hRM_PHONE.FIRST_NME;
                var positionTitle = hRM_PHONE.TITLE_DSC;
                var emailAddress = hRM_PHONE.EMAIL_ID;
                var section = hRM_PHONE.SECTION_DSC;
                var location = hRM_PHONE.LOCATION_DSC;
                var floor = hRM_PHONE.FLOOR;
                var workNumber = hRM_PHONE.WORK_PHN;
                var faxNumber = hRM_PHONE.FAX_PHN;
                var pagerNumber = hRM_PHONE.PAGER_PHN;
                var cellNumber = hRM_PHONE.CELL_PHN;
                var civicNumber = hRM_PHONE.CIVIC_NBR;

                //get the street data from cache
                var streets = StaticCache.GetOpenDataStreets();

                var streetInfo = streets.Find(x => x.Value == hRM_PHONE.STR_CODE.ToString());

                var civicAddress = civicNumber;

                if (streetInfo != null)
                {
                    civicAddress = civicAddress + " " + streetInfo.Text;
                }

                if (businessUnit != null)
                {
                    businessUnitDescription = businessUnit.BUSUNIT_DSC;
                }

                var body = "<html><body><table>";

                body = body + "<tr><td>Last Name:</td><td>" + lastName + "</td></tr>" +
                            "<tr><td>First Name:</td><td>" + firstName + "</td></tr>" +
                            "<tr><td>Title:</td><td>" + positionTitle + "</td></tr>" +
                            "<tr><td>Email Address:</td><td>" + emailAddress + "</td></tr>" +
                            "<tr><td>Section:</td><td>" + section + "</td></tr>" +
                            "<tr><td>Work Location:</td><td>" + location + "</td></tr>" +
                            "<tr><td>Floor:</td><td>" + floor + "</td></tr>" +

                            // if Street is null then return " " else return Street
                            "<tr><td>Civic Address:</td><td>" + civicAddress + "</td></tr>" +

                            // if Business Unit is null return " " else return the Description
                            "<tr><td>Business Unit:</td><td>" + businessUnitDescription + "</td></tr>" +
                            "<tr><td>Work Phone:</td><td>" + workNumber + "</td></tr>" +
                            "<tr><td>Fax Number:</td><td>" + faxNumber + "</td></tr>" +
                            "<tr><td>Cell Number:</td><td>" + cellNumber + "</td></tr>" +
                            "<tr><td>Pager Number:</td><td>" + pagerNumber + "</td></tr>";

                body = body + "</table></body></html>";

                var message = new MailMessage();
                using (var smtp = new SmtpClient())
                {
                    var fromAddress = ConfigurationManager.AppSettings["emailFromAddress"];
                    var toAddress = ConfigurationManager.AppSettings["emailToAddress"];

                    message.From = new MailAddress(fromAddress);
                    message.To.Add(new MailAddress(toAddress));
                    message.Subject = subject;
                    message.IsBodyHtml = true; //to make message body as html  
                    message.Body = body;
                    smtp.Port = Convert.ToInt16(smtpPort);
                    smtp.Host = smtpMailer; //for mail host  
                    //smtp.Host = "smarthost.halifax.ca";
                    smtp.EnableSsl = false;
                    smtp.UseDefaultCredentials = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(message);
                }

                var mailSent = true;
                return RedirectToAction(nameof(Sent), new { mailSent = mailSent });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Sent(bool mailSent)
        {
            try
            {
                ViewBag.EmailSent = mailSent;

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: HRM_PHONE/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                if (id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var repo = new PhoneBookRepository();
                var viewModel = PhoneBookRepository.Find(id);

                if (viewModel.SEQ_ID == 0)
                {
                    return HttpNotFound();
                }

                viewModel.BusinessUnits = BusinessUnitsRepository.GetBusinessUnits();
                viewModel.Streets = StreetsRepository.GetStreets();

                viewModel.PageMode = nameof(Delete);
                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: HRM_PHONE/Delete/5
        [HttpPost, ActionName(nameof(Delete))]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                var hRM_PHONE = db.HRM_PHONE.Find(id);

                if (IsPhoneBookAdmin())
                {
                    db.HRM_PHONE.Remove(hRM_PHONE);
                    db.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    var repo = new PhoneBookRepository();
                    var viewModel = PhoneBookRepository.Find(id);

                    if (viewModel.SEQ_ID == 0)
                    {
                        return HttpNotFound();
                    }

                    viewModel.BusinessUnits = BusinessUnitsRepository.GetBusinessUnits();
                    viewModel.Streets = StreetsRepository.GetStreets();

                    viewModel.PageMode = nameof(Delete);
                    viewModel.ActionMode = "DELETE";
                    return RedirectToAction(nameof(Review), viewModel);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
            db.Dispose();
            db.Dispose();
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            //Logging the Exception
            filterContext.ExceptionHandled = true;


            var Result = this.View("Error", new HandleErrorInfo(exception,
                filterContext.RouteData.Values["controller"].ToString(),
                filterContext.RouteData.Values["action"].ToString()));

            filterContext.Result = Result;

        }

    }
}
