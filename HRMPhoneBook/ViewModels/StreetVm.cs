﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMPhoneBook.ViewModels
{
    public class StreetVM
    {
        public string StreetId { get; set; }
        public string StreetName { get; set; }
    }
}