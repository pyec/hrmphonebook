﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HRMPhoneBook.ViewModels
{
    public class HRMPhoneVM
    {
        [Required]
        public int SEQ_ID { get; set; }

        [Display(Name = "Employee Id")]
        public string EMPL_ID { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LAST_NME { get; set; }

        [Display(Name = "First Name")]
        public string FIRST_NME { get; set; }

        [Display(Name = "Title")]
        public string TITLE_DSC { get; set; }

        [Display(Name = "Business Unit Description")]
        public string BUNIT_DSC { get; set; }

        [Display(Name = "Division")]
        public string DIVISION_DSC { get; set; }

        [Display(Name = "Section")]
        public string SECTION_DSC { get; set; }

        [Display(Name = "Work Location")]
        public string LOCATION_DSC { get; set; }

        [Display(Name = "Work Phone")]
        public string WORK_PHN { get; set; }

        [Display(Name = "Fax")]
        public string FAX_PHN { get; set; }

        [Display(Name = "Cell")]
        public string CELL_PHN { get; set; }

        [Display(Name = "Pager")]
        public string PAGER_PHN { get; set; }

        [Display(Name = "Update Date")]
        public DateTime? UPDATE_DTE { get; set; }

        [Display(Name = "Email")]
        public string EMAIL_ID { get; set; }

        [Display(Name = "Business Unit")]
        public int? BUSUNIT_CDE { get; set; }
        public string BusinessUnitDescription { get; set; }
        public IEnumerable<SelectListItem> BusinessUnits { get; set; }

        [Display(Name = "Street")]
        public int? STR_CODE { get; set; }
        public IEnumerable<SelectListItem> Streets { get; set; }

        [Display(Name = "Civic Number")]
        public string CIVIC_NBR { get; set; }

        [Display(Name = "Floor")]
        public string FLOOR { get; set; }

        public string PageMode { get; set; }

        public string ActionMode { get; set; }

        public string ErrorMessage { get; set; }

        public string PageMessage { get; set; }
    }
}