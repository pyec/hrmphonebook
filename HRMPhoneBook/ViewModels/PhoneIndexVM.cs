﻿using System;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HRMPhoneBook.ViewModels
{
    public class PhoneIndexVM
    {
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Business Unit")]
        public string SelectedBusinessUnit { get; set; }
        public IEnumerable<SelectListItem> BusinessUnits { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Section")]
        public string Section { get; set; }

        [Display(Name = "Work Phone")]
        public string WorkPhone { get; set; }

        [Display(Name = "Cell")]
        public string Cell { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Pager")]
        public string Pager { get; set; }

        [Display(Name = "Work Location")]
        public string WorkLocation { get; set; }

        [Display(Name = "Civic Number")]
        public string CivicNumber { get; set; }

        [Display(Name = "Street")]
        public string SelectedStreet { get; set; }
        public IEnumerable<SelectListItem> Streets { get; set; }

        [Display(Name = "Floor")]
        public string Floor { get; set; }

        public List<HRMPhoneVM> SearchResults { get; set; }
    }
}
