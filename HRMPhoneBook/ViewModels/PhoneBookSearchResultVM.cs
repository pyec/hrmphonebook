﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace HRMPhoneBook.ViewModels
{
    public class PhoneBookSearchResultVM
    {
        public IEnumerable<HRMPhoneVM> HRMPhones { get; set; }
    }
}